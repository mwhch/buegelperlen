var rowsOfAvgColors = [];
var selectedColors = [];

function convertImage(){
    //initialize stuff...
    var canvas = document.createElement('canvas');
    var img = new Image();
    img.onload = function() {
        alert(this.width+" "+this.height);
    }
    canvas.width = document.getElementById('previewImg').width;
    canvas.height= document.getElementById('previewImg').height;
    console.log("canvas width: "+canvas.width+", canvas height: "+canvas.height);
    var ctx = canvas.getContext('2d');
    // copy image data into canvas
    var myImgElement = document.getElementById('previewImg');
    ctx.drawImage(myImgElement, 0, 0);
    // how many segments and what size do they have
    var amountSegmentsX = document.getElementById("amountX").value;
    var amountSegmentsY = document.getElementById("amountY").value;
    var segmentSize = {};
    segmentSize["x"] = Math.floor(document.getElementById('previewImg').width / amountSegmentsX);
    segmentSize["y"] = Math.floor(document.getElementById('previewImg').height / amountSegmentsY);
    console.log(amountSegmentsX+" segments with each "+segmentSize["x"]+" pixel width and "+segmentSize["y"]+" pixel height");
    for(var segmentNumberY = 0; segmentNumberY < amountSegmentsY; segmentNumberY++) {
        var colorsOfSegmentsInRow = []; //array to store to colors of the current rows segments
        for(var segmentNumberX = 0; segmentNumberX < amountSegmentsX; segmentNumberX++) {
            var sumOfColors = {"r":0, "g":0, "b":0, "a":0};
            //proccess average color of the current segment
            for (var pixelOfCurrentSegmentY = 0; pixelOfCurrentSegmentY < segmentSize["y"]; pixelOfCurrentSegmentY++) {
                for (var pixelOfCurrentSegmentX = 0; pixelOfCurrentSegmentX < segmentSize["x"]; pixelOfCurrentSegmentX++) {
                    var currentPixelColor = ctx.getImageData((segmentNumberX*segmentSize["x"]+pixelOfCurrentSegmentX), (segmentNumberY*segmentSize["y"]+pixelOfCurrentSegmentY), 1, 1).data;
                    sumOfColors["r"] += currentPixelColor[0];
                    sumOfColors["g"] += currentPixelColor[1];
                    sumOfColors["b"] += currentPixelColor[2];
                    sumOfColors["a"] += currentPixelColor[3];
                }
                var amountPixelsPerSegment = segmentSize["x"]*segmentSize["y"];
                var avgColors = {"r":(Math.ceil(sumOfColors["r"]/amountPixelsPerSegment)), 
                                 "g":(Math.ceil(sumOfColors["g"]/amountPixelsPerSegment)),
                                 "b":(Math.ceil(sumOfColors["b"]/amountPixelsPerSegment)),
                                 "a":(Math.ceil(sumOfColors["a"]/amountPixelsPerSegment))};
            }
            // convert colors into a hex string by checking if in only needs one digit - if so: add a "0" in front so there are always 6 hex digits for each color
            // https://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
            var avgHex = {};
            if(avgColors["r"].toString(16).length == 1) {
                avgHex["r"] = ""+0+""+avgColors["r"].toString(16);
            } else {
                avgHex["r"] = avgColors["r"].toString(16);
            }
            if(avgColors["g"].toString(16).length == 1) {
                avgHex["g"] = ""+0+""+avgColors["g"].toString(16);
            } else {
                avgHex["g"] = avgColors["g"].toString(16);
            }
            if(avgColors["b"].toString(16).length == 1) {
                avgHex["b"] = ""+0+""+avgColors["b"].toString(16);
            } else {
                avgHex["b"] = avgColors["b"].toString(16);
            }
            var avgHexString = "#"+avgHex["r"]+avgHex["g"]+avgHex["b"];
            //add average color of the segment [oooo...]
            //colorsOfSegmentsInRow.push(avgHexString);
            colorsOfSegmentsInRow.push(avgColors);
            console.log("added color "+avgHexString+" for segment "+segmentNumberX+"|"+segmentNumberY);
            console.log(colorsOfSegmentsInRow);
        }
        //add row of avg colors of the segments to the rows processed yet
        // oooooo
        // oooooo
        // ...
        rowsOfAvgColors.push(colorsOfSegmentsInRow);        
    }
    //gaining colors done at this point
    drawSegments();
    
}

function drawSegments() {
    var segmentSize = {};
    // how many segments and what size do they have
    var amountSegmentsX = document.getElementById("amountX").value;
    var amountSegmentsY = document.getElementById("amountY").value;
    segmentSize["x"] = Math.floor(document.getElementById('previewImg').width / amountSegmentsX);
    segmentSize["y"] = Math.floor(document.getElementById('previewImg').height / amountSegmentsY);
    //drawing picture now
    var resultCanvas = document.getElementById("resultCanvas");
   // resultCanvas.width = document.getElementById('previewImg').width;
   // resultCanvas.height= document.getElementById('previewImg').height;
    var perlenRadius = 6;
    var abstand = perlenRadius;
    var transparencyThreshhold = 100; //segment with average alpha channel below value won't be displayed
    resultCanvas.width = amountSegmentsX*perlenRadius*2;
    resultCanvas.height= amountSegmentsY*perlenRadius*2;
    var resultCtx = resultCanvas.getContext('2d');
    resultCtx.lineWidth = 3;
    //var perlenRadius=Math.floor(segmentSize["x"]/2);
    for(var currentRow = 0; currentRow < amountSegmentsY; currentRow++) {
        for(var segment = 0; segment < amountSegmentsX; segment++) {
            if(rowsOfAvgColors[currentRow][segment]["a"] >= transparencyThreshhold) {
                resultCtx.beginPath();
                resultCtx.strokeStyle= "rgba("+rowsOfAvgColors[currentRow][segment]["r"]+", "+rowsOfAvgColors[currentRow][segment]["g"]+", "+rowsOfAvgColors[currentRow][segment]["b"]+")";
                resultCtx.fillStyle= "rgba("+rowsOfAvgColors[currentRow][segment]["r"]+", "+rowsOfAvgColors[currentRow][segment]["g"]+", "+rowsOfAvgColors[currentRow][segment]["b"]+", "+rowsOfAvgColors[currentRow][segment]["a"]+")";
                resultCtx.arc((segment*perlenRadius*2+abstand), (currentRow*perlenRadius*2+abstand), perlenRadius, 0, 2*Math.PI);
                console.log("drew segment "+segment+"|"+currentRow+" with color: rgba("+rowsOfAvgColors[currentRow][segment]["r"]+", "+rowsOfAvgColors[currentRow][segment]["g"]+", "+rowsOfAvgColors[currentRow][segment]["b"]+", "+rowsOfAvgColors[currentRow][segment]["a"]+")");
                resultCtx.stroke();
            }
        }
    }
}

function previewFile() {
    var preview = document.getElementById("previewImg"); //selects the query named img
    preview.src="";
    var file    = document.getElementById("file").files[0]; //sames as here
    var reader  = new FileReader();
    
    reader.onloadend = function () {
        preview.src = reader.result;
    }

    if (file) {
        reader.readAsDataURL(file); //reads the data as a URL
    } else {
        preview.src = "";
    }
    var canvas = document.getElementById("resultCanvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0,0, canvas.width, canvas.height);
}

function selectedColor() {
}