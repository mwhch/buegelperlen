# buegelperlen

*Idea:*

**convert images to a iron-beads tutorial**
you select a file, how big your canvas is and what colors you have and you're being shown how to layout the picture you uploaded in iron-beads
This is done by dividing the selected picture in segments (according to the size you entered) and calculating the segments average color. Then each segments color is being matched to the colors that have been selected as beads that the user owns.

*status:*
* buegler.php: add function to insert file by url
* script.js: right now just the average color of the segments, **no matching to colors**; also: keeping the original pictures format would be nice
* style.css: *yawn*
